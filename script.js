const lista = document.getElementById("lista");
const txtInput = document.getElementById("txtInput");
const btn = document.getElementById("btn");
const spinner = document.getElementById("pos");
const editBtn = document.getElementById('editBtn');


function insereInput() {
    const element = document.createElement('li');
    element.innerText = txtInput.value;
    element.addEventListener('click', seRemove);
    
    const listSize = lista.childElementCount;
    const pos = parseInt(spinner.value);

    // Se por acaso o spinner não estiver definido, ou for maior do que o tamanho da lista, 
    // ou a lista estiver vazia, simplesmente da um append na lista.
    if (!pos ||  pos > listSize || listSize <= 0) {
        lista.append(element);
    } 
    // Se não, insere o elemento antes do filho com a posição especificada.
    else {
        lista.children[pos].insertAdjacentElement('beforebegin', element);
    }
    
    txtInput.value = '';
}

function seRemove(e) {
    e.target.remove();
}


function editItemAtPosition() {
    const pos = parseInt(spinner.value) || 0;
    const element = lista.children[pos];
    if (!element) return;

    element.innerHTML = txtInput.value;
}

btn.addEventListener('click', insereInput)
txtInput.addEventListener('keypress', (e) => {
    if (e.keyCode === 13) insereInput();
})

editBtn.addEventListener('click', editItemAtPosition)